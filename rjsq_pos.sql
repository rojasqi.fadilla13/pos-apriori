-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 23 Jan 2019 pada 03.35
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rjsq_pos`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `bid` varchar(15) NOT NULL,
  `bnama` varchar(45) DEFAULT NULL,
  `bsatuan` varchar(35) DEFAULT NULL,
  `bharga_pokok` double DEFAULT NULL,
  `bharga_jual` double DEFAULT NULL,
  `bharga_jual_grosir` double DEFAULT NULL,
  `bstok` int(11) NOT NULL,
  `btgl_masuk` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `bkategori_id` int(11) DEFAULT NULL,
  `buser_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`bid`, `bnama`, `bsatuan`, `bharga_pokok`, `bharga_jual`, `bharga_jual_grosir`, `bstok`, `btgl_masuk`, `bkategori_id`, `buser_id`) VALUES
('BRG-004', 'Indomie Rasa Empal Gentong', 'BOX', 3500, 98500, 3500, 96, '2018-11-14 04:09:47', 2, NULL),
('BRG-005', 'Indomie Goreng Rasa Dendeng Balado', 'BOX', 3500, 89000, 3200, 151, '2018-11-14 04:12:06', 2, NULL),
('BRG-006', 'Sedaap Rasa Ayam Bawang', 'BOX', 2800, 87500, 3000, 44, '2018-11-14 04:15:34', 2, NULL),
('BRG-007', 'Sedaap Rasa Kari Spesial', 'BOX', 3000, 92000, 3300, 72, '2018-11-14 04:17:05', 2, NULL),
('BRG-008', 'Bodrex Migran', 'PCS', 2000, 37500, 2500, 86, '2018-11-14 04:19:39', 1, NULL),
('BRG-009', 'Panadol Paracetamol', 'PCS', 1500, 40000, 2650, 78, '2018-11-14 04:20:36', 1, NULL),
('BRG-010', 'Mixagrip Flu', 'PCS', 2700, 38200, 2200, 260, '2018-11-14 04:21:42', 1, NULL),
('BRG-011', 'Paramex Sakit Kepala', 'PCS', 2800, 43500, 3000, 259, '2018-11-14 04:22:42', 1, NULL),
('BRG-012', 'Tramadol', 'PCS', 2000, 36600, 2750, 166, '2018-12-07 13:52:05', 1, NULL),
('BRG-013', 'Shampo Sunsilk', 'PCS', 1000, 40200, 2000, 192, '2018-12-23 17:19:26', 6, NULL),
('BRG-014', 'Masako Rasa Ayam', 'PCS', NULL, 51500, 1000, 200, '2019-01-05 16:04:07', 4, NULL),
('BRG-015', 'Aqua Botol 500ml', 'BOX', NULL, 125000, 3250, 28, '2019-01-05 16:46:56', 5, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(80) NOT NULL,
  `ig` varchar(30) NOT NULL,
  `desc` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `website` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `contact`
--

INSERT INTO `contact` (`id`, `nama`, `email`, `ig`, `desc`, `alamat`, `website`) VALUES
(1, 'RJSQ - Point Of Sales with Apriori Method', 'rojasqi.fadilla13@gmail.com', '@rojasqifadilla13', 'Terdepan, Terpercaya, Tumbuh Bersama Anda', 'Jl. Sarijadi No.54 Sariasih Kota Bandung 40511', 'www.toko-rjsq.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pembelian`
--

CREATE TABLE `detail_pembelian` (
  `dbeli_id` int(11) NOT NULL,
  `dbeli_nofaktur` varchar(15) DEFAULT NULL,
  `dbeli_barang_id` varchar(15) DEFAULT NULL,
  `dbeli_harga` double DEFAULT NULL,
  `dbeli_jumlah` int(11) DEFAULT NULL,
  `dbeli_total` double DEFAULT NULL,
  `dbeli_kode` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_penjualan`
--

CREATE TABLE `detail_penjualan` (
  `id_dj` int(11) NOT NULL,
  `pj_id` varchar(15) NOT NULL,
  `djual_bkategori_id` int(11) DEFAULT NULL,
  `djual_bid` varchar(15) DEFAULT NULL,
  `djual_bnama` varchar(45) DEFAULT NULL,
  `djual_qty` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_penjualan`
--

INSERT INTO `detail_penjualan` (`id_dj`, `pj_id`, `djual_bkategori_id`, `djual_bid`, `djual_bnama`, `djual_qty`) VALUES
(2, 'PJ-002', NULL, 'BRG-006', NULL, 12),
(3, 'PJ-002', NULL, 'BRG-005', NULL, 12),
(7, 'PJ-003', NULL, 'BRG-004', NULL, 10),
(8, 'PJ-004', NULL, 'BRG-004', NULL, 10),
(9, 'PJ-004', NULL, 'BRG-005', NULL, 6),
(10, 'PJ-004', NULL, 'BRG-010', NULL, 10),
(11, 'PJ-004', NULL, 'BRG-011', NULL, 11),
(12, 'PJ-005', NULL, 'BRG-007', NULL, 8),
(13, 'PJ-005', NULL, 'BRG-010', NULL, 6),
(14, 'PJ-005', NULL, 'BRG-008', NULL, 6),
(15, 'PJ-006', NULL, 'BRG-005', NULL, 5),
(16, 'PJ-006', NULL, 'BRG-006', NULL, 4),
(18, 'PJ-007', NULL, 'BRG-006', NULL, 4),
(20, 'PJ-008', NULL, 'BRG-008', NULL, 3),
(21, 'PJ-009', NULL, 'BRG-006', NULL, 3),
(22, 'PJ-009', NULL, 'BRG-012', NULL, 4),
(23, 'PJ-010', NULL, 'BRG-006', NULL, 2),
(24, 'PJ-010', NULL, 'BRG-013', NULL, 2),
(25, 'PJ-010', NULL, 'BRG-010', NULL, 4),
(26, 'PJ-010', NULL, 'BRG-005', NULL, 4),
(27, 'PJ-011', NULL, 'BRG-006', NULL, 3),
(28, 'PJ-011', NULL, 'BRG-009', NULL, 12),
(29, 'PJ-012', NULL, 'BRG-008', NULL, 5),
(30, 'PJ-013', NULL, 'BRG-015', NULL, 5),
(31, 'PJ-013', NULL, 'BRG-006', NULL, 6),
(32, 'PJ-013', NULL, 'BRG-009', NULL, 10),
(33, 'PJ-014', NULL, 'BRG-015', NULL, 2),
(34, 'PJ-014', NULL, 'BRG-006', NULL, 2),
(35, 'PJ-014', NULL, 'BRG-013', NULL, 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `kategori_id` int(11) NOT NULL,
  `kategori_nama` varchar(35) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`kategori_id`, `kategori_nama`) VALUES
(1, 'Obat'),
(2, 'Makanan'),
(4, 'Bumbu Dapur'),
(5, 'Minuman'),
(6, 'Perlengkapan Mandi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembelian`
--

CREATE TABLE `pembelian` (
  `pb_kode` varchar(15) NOT NULL,
  `pb_nofaktur` varchar(15) DEFAULT NULL,
  `pb_tanggal` date DEFAULT NULL,
  `pb_supplier_id` varchar(15) DEFAULT NULL,
  `pb_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjualan`
--

CREATE TABLE `penjualan` (
  `pj_id` varchar(15) NOT NULL,
  `pj_tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `pj_total` double DEFAULT NULL,
  `pj_jumlah_uang` double DEFAULT NULL,
  `pj_kembalian` double DEFAULT NULL,
  `pj_supplier_id` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penjualan`
--

INSERT INTO `penjualan` (`pj_id`, `pj_tanggal`, `pj_total`, `pj_jumlah_uang`, `pj_kembalian`, `pj_supplier_id`) VALUES
('PJ-008', '2019-01-02 17:00:00', 9000, 10000, 1000, 'SUP-003'),
('PJ-009', '2019-01-02 17:00:00', 22500, 30000, 7500, 'SUP-002'),
('PJ-010', '2019-01-02 17:00:00', 30000, 31000, 1000, 'SUP-002'),
('PJ-011', '2019-01-02 17:00:00', 40500, 50000, 9500, 'SUP-002'),
('PJ-012', '2019-01-02 17:00:00', 15000, 50000, 35000, 'SUP-001'),
('PJ-013', '2019-01-05 17:00:00', 1550000, 2000000, 450000, 'SUP-004'),
('PJ-014', '2019-01-07 17:00:00', 666200, 700000, 33800, 'SUP-003');

-- --------------------------------------------------------

--
-- Struktur dari tabel `retur`
--

CREATE TABLE `retur` (
  `rid` int(11) NOT NULL,
  `rtanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `rbarang_id` varchar(15) DEFAULT NULL,
  `rbarang_nama` varchar(50) DEFAULT NULL,
  `rbarang_satuan` varchar(30) DEFAULT NULL,
  `rharga_jual` double DEFAULT NULL,
  `rqty` int(11) DEFAULT NULL,
  `rsubtotal` double DEFAULT NULL,
  `rketerangan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `retur`
--

INSERT INTO `retur` (`rid`, `rtanggal`, `rbarang_id`, `rbarang_nama`, `rbarang_satuan`, `rharga_jual`, `rqty`, `rsubtotal`, `rketerangan`) VALUES
(40, '2019-01-02 07:10:47', 'BRG-004', 'Indomie Rasa Empal Gentong', 'BOX', 4000, 3, 12000, 'Rusak'),
(51, '2019-01-02 08:36:06', 'BRG-004', 'Indomie Rasa Empal Gentong', 'BOX', 4000, 1, 4000, 'Rusak'),
(52, '2019-01-02 08:36:52', 'BRG-011', 'Paramex Sakit Kepala', 'PCS', 3500, 1, 3500, 'Rusak'),
(54, '2019-01-02 08:50:36', 'BRG-005', 'Indomie Goreng Rasa Dendeng Balado', 'BOX', 4000, 1, 4000, 'Rusak'),
(55, '2019-01-02 08:51:06', 'BRG-009', 'Panadol Paracetamol', 'PCS', 2500, 4, 10000, 'Rusak');

-- --------------------------------------------------------

--
-- Struktur dari tabel `suplier`
--

CREATE TABLE `suplier` (
  `sid` varchar(15) NOT NULL,
  `snama` varchar(35) DEFAULT NULL,
  `salamat` varchar(100) DEFAULT NULL,
  `stelp` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `suplier`
--

INSERT INTO `suplier` (`sid`, `snama`, `salamat`, `stelp`) VALUES
('SUP-001', 'Ujang Mamprang', 'Jl.Nyasar Gg.Buntu Rt:04/04 No.102 Kec.Cimenyan Purwakarta', '082123456789'),
('SUP-002', 'Mumun Karmila', 'Jl.Cigondewah No.22 Rt:02/04 Kec.Sukaluyu Bogor', '02212365'),
('SUP-003', 'Solih Solihin', 'Jl.Cipatewew Raya No.332 Rt:09/08 Kec.Kebonkawung Rancaekek', '083214747476'),
('SUP-004', 'Jonathan Bauman', 'Jl.Sukabumi no.99 rt:01/02 kec.sukijan kel.kemajen Bandung', '085974784842');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `nama` varchar(35) DEFAULT NULL,
  `username` varchar(35) DEFAULT NULL,
  `password` varchar(35) DEFAULT NULL,
  `user_level` enum('Admin','Kasir') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`user_id`, `nama`, `username`, `password`, `user_level`) VALUES
(1, 'Rojasqi Fadilla', 'admin', '202cb962ac59075b964b07152d234b70', 'Admin'),
(2, 'Dadangxs', 'kasir', 'caf1a3dfb505ffed0d024130f58c5cfa', 'Kasir'),
(8, 'Manon', 'manonsiadmin', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'Admin'),
(9, 'Sodik', 'sodiksikasir', 'a01610228fe998f515a72dd730294d87', 'Kasir'),
(10, 'Susay Manoy', 'Suasay', 'caf1a3dfb505ffed0d024130f58c5cfa', 'Kasir');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`bid`),
  ADD KEY `bkategori_id` (`bkategori_id`),
  ADD KEY `buser_id` (`buser_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_pembelian`
--
ALTER TABLE `detail_pembelian`
  ADD PRIMARY KEY (`dbeli_id`),
  ADD KEY `dbeli_nofaktur` (`dbeli_nofaktur`),
  ADD KEY `dbeli_barang_id` (`dbeli_barang_id`),
  ADD KEY `dbeli_kode` (`dbeli_kode`);

--
-- Indexes for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD PRIMARY KEY (`id_dj`),
  ADD KEY `djual_nofaktur` (`djual_bkategori_id`,`djual_bid`),
  ADD KEY `djual_bid` (`djual_bid`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`kategori_id`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`pb_kode`),
  ADD KEY `pb_kode` (`pb_kode`,`pb_supplier_id`,`pb_user_id`),
  ADD KEY `pb_user_id` (`pb_user_id`),
  ADD KEY `pb_supplier_id` (`pb_supplier_id`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`pj_id`),
  ADD KEY `pj_user_id` (`pj_supplier_id`);

--
-- Indexes for table `retur`
--
ALTER TABLE `retur`
  ADD PRIMARY KEY (`rid`);

--
-- Indexes for table `suplier`
--
ALTER TABLE `suplier`
  ADD PRIMARY KEY (`sid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `detail_pembelian`
--
ALTER TABLE `detail_pembelian`
  MODIFY `dbeli_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  MODIFY `id_dj` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `kategori_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `retur`
--
ALTER TABLE `retur`
  MODIFY `rid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD CONSTRAINT `barang_ibfk_1` FOREIGN KEY (`buser_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `barang_ibfk_2` FOREIGN KEY (`bkategori_id`) REFERENCES `kategori` (`kategori_id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Ketidakleluasaan untuk tabel `detail_pembelian`
--
ALTER TABLE `detail_pembelian`
  ADD CONSTRAINT `detail_pembelian_ibfk_1` FOREIGN KEY (`dbeli_barang_id`) REFERENCES `barang` (`bid`),
  ADD CONSTRAINT `detail_pembelian_ibfk_2` FOREIGN KEY (`dbeli_kode`) REFERENCES `pembelian` (`pb_kode`) ON DELETE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD CONSTRAINT `detail_penjualan_ibfk_1` FOREIGN KEY (`djual_bid`) REFERENCES `barang` (`bid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `detail_penjualan_ibfk_2` FOREIGN KEY (`djual_bkategori_id`) REFERENCES `kategori` (`kategori_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pembelian`
--
ALTER TABLE `pembelian`
  ADD CONSTRAINT `pembelian_ibfk_1` FOREIGN KEY (`pb_user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pembelian_ibfk_2` FOREIGN KEY (`pb_supplier_id`) REFERENCES `suplier` (`sid`);

--
-- Ketidakleluasaan untuk tabel `penjualan`
--
ALTER TABLE `penjualan`
  ADD CONSTRAINT `penjualan_ibfk_1` FOREIGN KEY (`pj_supplier_id`) REFERENCES `suplier` (`sid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
