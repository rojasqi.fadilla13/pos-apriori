
<div class="container">
	<div class="card">
		<h4 class="alert alert-warning" style="text-align: center"><font color="#000000">Keterangan</font></h4>
		<div class="row-fluid">
			<?php if(isset($dt_penjualan)){
                foreach($dt_penjualan as $row){
                    ?>
                    <div class="card-body">
                    	<div class="card-text">
                        <dl class="row">
                            <dt class="col-2"><font color="#F79346">Kode Penjualan :</font></dt> <dd class="col-2"><font color="#ffffff"><i><?php echo $row->pj_id?></i></font></dd>
                            
                            <dt class="col-2"><font color="#F79346">Tanggal Penjualan :</font></dt> <dd class="col-2"><font color="#ffffff"><i><?php echo date("d M Y",strtotime($row->pj_tanggal));?></i></font></dd>
                            
                            <dt class="col-2"><font color="#F79346">Total Harga :</font></dt> <dd class="col-2"><u><font color="#ffffff"><i><?= currency_format($row->pj_total); ?></i></font></u></dd>
                            <br>
                            <dt class="col-2"><font color="#F79346">Jumlah Tunai :</font></dt>
                            <dd class="col-2"><font color="#ffffff"><i><?= currency_format($row->pj_jumlah_uang);?></i></font></dd>

                            <dt class="col-2"><font color="#F79346">Kembalian :</font></dt>
                            <dd class="col-2"><font color="#ffffff"><i><?= currency_format($row->pj_kembalian);?></i></font></dd>
                        </dl>
                    </div>
                </div>
                <br>
                    <div class="card-body">
                    	<div class="card-text">
                        <dl class="row">
                            <dt class="col-2"><font color="#F79346">Nama Supplier :</font></dt>
                            <dd class="col-2"><font color="#ffffff"><i><?php echo $row->snama?></i></font></dd>
                            
                            <dt class="col-2"><font color="#F79346">Alamat Supplier :</font></dt>
                            <dd class="col-2"><font color="#ffffff"><i><?php echo $row->salamat?></i></font></dd>
                            
                            <dt class="col-2"><font color="#F79346">Telp Supplier :</font></dt>
                            <dd class="col-2"><font color="#ffffff"><i><?php echo $row->stelp?></i></font></dd>
                        </dl>
                    </div>
                </div>
                <?php }
            }
            ?>
        </div>
    </div>


    <div class="card">
        <h4 class="alert alert-warning" style="text-align: center"><font color="#000000">Daftar Barang</font></h4>
        <div class="row-fluid">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th bgcolor="#ffffff"><font color="#000000"><center>No</center></th>
                    <th bgcolor="#ffffff"><font color="#000000"><center>Kode Barang</center></th>
                    <th bgcolor="#ffffff"><font color="#000000"><center>Nama Barang</center></th>
                    <th bgcolor="#ffffff"><font color="#000000"><center>Kategori</center></th>
                    <th bgcolor="#ffffff"><font color="#000000"><center>Qty</center></th>
                    <th bgcolor="#ffffff"><font color="#000000"><center>Harga</center></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $no=1;
                if(isset($barang_jual)){
                    foreach($barang_jual as $row ){
                        ?>
                        <tr>
                            <td><center><?php echo $no++; ?></center></td>
                            <td><center><?php echo $row->djual_bid?></center></td>
                            <td><center><?php echo $row->bnama?></center></td>
                            <td><center><?php echo $row->kategori_nama?></center></td>
                            <td><center><?php echo $row->djual_qty?></center></td>
                            <td><center><?php echo currency_format($row->bharga_jual)?></center></td>
                        </tr>
                    <?php }
                }
                ?>
                </tbody>
            </table>

            <div class="form-actions">
                <a href="<?php echo site_url('penjualan')?>" class="btn btn-warning">
                    <i class="tim-icons icon-user-run"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>