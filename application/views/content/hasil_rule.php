<html>
<head>
	<title>Hasil Rule Association</title>
</head>
<body>
<div class="container">

<div class="panel panel-default">
	<div class="panel-heading">
		<?php echo "Tanggal ".$awal." - ".$akhir."";?>
		<!-- //echo "Tanggal : 01-12-2017 - 01-12-2017 <input type='submit' class='btn btn-primary' name='cetak' value='Cetak'>"; -->
	</div>
	<br>
	<div class="panel-body">
		<h3>Kombinasi 3 Produk Itemset</h3>
		<a class="btn btn-primary btn-sm pull-right" href="javascript:window.print()" target="_blank"><i class="fa fa-print"></i> Print</a>
		<br>
		<br>
		<table id="" class="table table-striped table-hover">
			<tr>
				<td width="250">
					Minimum Support
				</td>
				<td width="50">
					:
				</td>
				<td align="left">
					<?php echo $support;?>
				</td>
			</tr>
			<tr>
				<td>
					Minimum Support x Confidence
				</td>
				<td>
					:
				</td>
				<td>
					<?php echo $sxc;?>
				</td>
			</tr>
		</table>

		<form action="" method="post">
			<div class="table">
				<table id="" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th style="text-align: center; background-color: white; width: 400px"><font style="color: black">Aturan</font></th>
							<th style="text-align: center; background-color: white"><font style="color: black">Support</font></th>
							<th style="text-align: center; background-color: white"><font style="color: black">Confidence</font></th>
							<th style="text-align: center; background-color: white"><font style="color: black">Support x Confidence</th>
							<th style="text-align: center; background-color: white"><font style="color: black">Memenuhi Thresold Support</font></th>
							<th style="text-align: center; background-color: white"><font style="color: black">Memenuhi Thresold Support x Confidence</font></th>
						</tr>
					</thead>
					<tbody>
						<?php

							for($o=0;$o<count($listKode);$o++){

						?>
						<tr>
							<td>
								<?php echo "Jika Membeli ".($listName[$o][0])." dan ".($listName[$o][1])." Maka Akan Membeli ".($listName[$o][2]);?>
							</td>
							<td style="text-align: center">
							  	<?php print $listSupport[$o][0];?>
							</td>
							<td style="text-align: center">
							  	<?php print $listConfidence[$o];?>
							</td>
							<td style="text-align: center">
								<?php print $listRsxc[$o][0];?>
							</td>
							<td style="text-align: center">
								<?php print ($listSupport[$o][1] == 1) ? "Ya" : "Tidak";?>
							</td>
							<td style="text-align: center">
								<?php print ($listRsxc[$o][1] == 1) ? "Ya" : "Tidak";?>
							</td>
						</tr>
						<?php
							}
						?>
					</tbody>
				</table>
			</div>

<!-- 			<?php //echo "<p align='center';float='center'>
								//<input type='submit' class='btn btn-primary' name='proses' value='Proses'>
								//<button type='reset' class='btn btn-danger' onclick='history.go(-1);'>Kembali</button></p>"
			//;
			?> -->
		</form>
		<br>
<!-- 		<?php
			//for($o=0;$o<count($listKode);$o++){
				//echo $listKode[$o][0]."=>";
				//echo $listKode[$o][1]."<br>";
			//}
		?> -->
		<center><h3>Hasil</h3></center>
		<br>
		<form action="" method="post">
			<div class="table">
				<table id="" class="table table-striped table-bordered table-hover">
					<tbody>
						<tr>
							<td colspan="4"><center>
								Maka Dapat Disimpulkan Pola Perilaku Komsumen Terhadap Pembelian Sebuah Produk Dapat Dilihat Dari Kemungkinan Terbesar Nilai Diatas Minumun Support dan Minimum Support x Confidance.
							</center></td>
						</tr>
					</tbody>
				</table>
			</div>

			<?php echo "<p align='center';float='center'>
								<button type='reset' class='btn btn-danger' onclick='history.go(-1);'>Kembali</button></p>"
			;
			?>
		</form>
	</div>
</div>
</div>
</body>
</html>