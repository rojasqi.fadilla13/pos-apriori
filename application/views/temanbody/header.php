<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/img/apple-icon.png')?>"/>
  <link rel="icon" type="image/png" href="<?php echo base_url('assets/img/favicon.png')?>"/>
  <title>
    <?php echo $title?>
  </title>

  <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet" />
  <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" />
  <link href="<?php echo base_url('assets/css/nucleo-icons.css')?>" rel="stylesheet" />
  <link href="<?php echo base_url('assets/css/blk-design-system.css?v=1.0.0"')?>" rel="stylesheet" />
  <link href="<?php echo base_url('assets/css/dataTables.bootstrap.min.css')?>" rel="stylesheet" />
  <link href="<?php echo base_url('assets/css/jquery.dataTables.min.css')?>" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css')?>" />
  
  <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.js')?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.js')?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/chosen.jquery.js');?>"></script>
  
</head>

<body>
<div class="container">
    <?php if ($this->session->userdata('LEVEL') == 'Admin'){ ?>
  <nav class="navbar navbar-expand-lg navbar-transparent" color-on-scroll="100>
              <div class="container">
                <div class="navbar-translate">
                  <a class="navbar-brand" href=""><strong><i>RJSQ•Point Of Sales</i></strong></a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#example-navbar-primary" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                  </button>
                </div>
                <div class="collapse navbar-collapse" id="example-navbar-primary">
                  <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                      <a class="nav-link" href="<?php echo site_url('dashboard')?>">
                        <i class="fa fa-home"></i> Dashboard
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<?php echo site_url('penjualan')?>">
                        <i class="tim-icons icon-cart"></i> Transaksi
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<?php echo site_url('retur')?>">
                        <i class="tim-icons icon-refresh-02"></i> Retur
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<?php echo site_url('master')?>">
                        <i class="tim-icons icon-bullet-list-67"></i> Master Data
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<?php echo site_url('laporan')?>">
                        <i class="tim-icons icon-notes"></i> Laporan
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<?php echo site_url('apriori')?>">
                        <i class="tim-icons icon-atom"></i> Apriori
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<?php echo site_url('login/logout')?>">
                        <i class="tim-icons icon-button-power"></i> Logout
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
<br>
    <?php } else { ?>

 <nav class="navbar navbar-expand-lg bg-success">
              <div class="container">
                <div class="navbar-translate">
                  <a class="navbar-brand" href=""><strong><i>RJSQ•Point Of Sales</i></strong></a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#example-navbar-primary" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                  </button>
                </div>
                <div class="collapse navbar-collapse" id="example-navbar-primary">
                  <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                      <a class="nav-link" href="<?php echo site_url('dashboard')?>">
                        <i class="fa fa-home"></i> Dashboard
                      </a>
                    </li>
                  <li class="nav-item">
                      <a class="nav-link" href="<?php echo site_url('penjualan')?>">
                        <i class="tim-icons icon-cart"></i> Transaksi
                      </a>
                    </li>
                  <li class="nav-item">
                      <a class="nav-link" href="<?php echo site_url('retur')?>">
                        <i class="tim-icons icon-refresh-02"></i> Retur
                      </a>
                    </li>
<!--                   <li class="nav-item">
                      <a class="nav-link" href="<?php echo site_url('content/master_supplier')?>">
                        <i class="tim-icons icon-refresh-02"></i> Supplier
                      </a>
                    </li> -->
                  <li class="nav-item">
                      <a class="nav-link" href="<?php echo site_url('login/logout')?>">
                        <i class="tim-icons icon-button-power"></i> Logout
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
<br>
<?php } ?>